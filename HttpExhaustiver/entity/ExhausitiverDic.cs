﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace HttpExhaustiver.entity
{
    class ExhausitiverDic
    {
        public string Param { get; set; }

        public String Path { get; set; }

        public StreamReader Reader { get; set; }
        public string Current { get; set; }
    }
}
